function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "row";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.row = Ti.UI.createTableViewRow({
        layout: "horizontal",
        id: "row"
    });
    $.__views.row && $.addTopLevelView($.__views.row);
    $.__views.icon = Ti.UI.createImageView({
        left: 0,
        height: 50,
        width: 50,
        id: "icon"
    });
    $.__views.row.add($.__views.icon);
    $.__views.catID = Ti.UI.createLabel({
        color: "#000",
        font: {
            fontFamily: "Ariel",
            fontSize: "20dp",
            fontWeight: "bold"
        },
        id: "catID"
    });
    $.__views.row.add($.__views.catID);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    $.icon.image = args.icon;
    $.catID.text = args.name;
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;